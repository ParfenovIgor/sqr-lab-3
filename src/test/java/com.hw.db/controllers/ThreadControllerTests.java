package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

class ThreadControllerTest {
    private ThreadController threadController;

    @BeforeEach
    void initTests() {
        threadController = new ThreadController();
    }

    @Test
    @DisplayName("Test CheckIdOrSlug")
    void testCheckIdOrSlug() {
        Thread thread1 = new Thread();
        thread1.setAuthor("Alice");
        thread1.setSlug("slug");
        thread1.setVotes(21);
        Thread thread2 = new Thread();
        thread2.setAuthor("Bob");
        thread2.setSlug("slug");
        thread2.setVotes(21);

        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAOMock.when(() -> ThreadDAO.getThreadById(21)).thenReturn(thread1);
            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread2);
            threadController.CheckIdOrSlug("21");
            Assertions.assertEquals("Alice", threadController.CheckIdOrSlug("21").getAuthor());
            Assertions.assertEquals("Bob", threadController.CheckIdOrSlug("slug").getAuthor());
        }
    }

    @Test
    @DisplayName("Test CreatePost")
    void testCreatePost() {
        Thread thread = new Thread();
        thread.setId(1);
        thread.setForum("forum");

        User user1 = new User();
        user1.setNickname("Alice");
        User user2 = new User();
        user2.setNickname("Bob");
        List<User> users = Arrays.asList(user1, user2);

        Post post1 = new Post();
        post1.setAuthor("Alice");
        post1.setMessage("message 1");
        Post post2 = new Post();
        post2.setAuthor("Bob");
        post2.setMessage("message 2");
        List<Post> posts = Arrays.asList(post1, post2);

        String slug = "slug";

        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userDAOMock = Mockito.mockStatic(UserDAO.class)) {
                threadDAOMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
                userDAOMock.when(() -> UserDAO.Info("Alice")).thenReturn(user1);
                userDAOMock.when(() -> UserDAO.Info("Bob")).thenReturn(user2);

                ResponseEntity response = threadController.createPost(slug, posts);

                Assertions.assertEquals(HttpStatus.CREATED, response.getStatusCode());
                Assertions.assertEquals(posts, response.getBody());
            }
        }
    }

    @Test
    @DisplayName("Test Posts")
    void testPosts() {
        Thread thread = new Thread();
        thread.setId(1);
        thread.setForum("forum");

        User user1 = new User();
        user1.setNickname("Alice");
        User user2 = new User();
        user2.setNickname("Bob");
        List<User> users = Arrays.asList(user1, user2);

        Post post1 = new Post();
        post1.setAuthor("Alice");
        post1.setMessage("message 1");
        Post post2 = new Post();
        post2.setAuthor("Bob");
        post2.setMessage("message 2");
        List<Post> posts = Arrays.asList(post1, post2);

        int limit = 24;
        int since = 11;
        String sort = "mrhn";
        Boolean desc = true;
        String slug = "slug";

        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userDAOMock = Mockito.mockStatic(UserDAO.class)) {
                threadDAOMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
                threadDAOMock.when(() -> ThreadDAO.getPosts(thread.getId(), limit, since, sort, desc)).thenReturn(posts);
                userDAOMock.when(() -> UserDAO.Info("Alice")).thenReturn(user1);
                userDAOMock.when(() -> UserDAO.Info("Bob")).thenReturn(user2);

                ResponseEntity response = threadController.Posts(slug, limit, since, sort, desc);

                Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
                Assertions.assertEquals(posts, response.getBody());
            }
        }
    }

    @Test
    @DisplayName("Test change")
    void testChange() {
        Thread thread = new Thread();
        thread.setId(1);
        thread.setForum("forum");

        String slug = "slug";

        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);

            ResponseEntity response = threadController.change(slug, new Thread());
            
            Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        }
    }

    @Test
    @DisplayName("Test info")
    void testInfo() {
        Thread thread = new Thread();
        thread.setId(1);
        thread.setForum("forum");

        String slug = "slug";

        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);

            ResponseEntity response = threadController.info(slug);

            Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
            Assertions.assertEquals(thread, response.getBody());
        }
    }

    @Test
    @DisplayName("Test createVote")
    void testCreateVote() {
        Thread thread = new Thread();
        thread.setId(1);
        thread.setForum("forum");
        thread.setVotes(10);

        String nickName = "Alice";
        Integer voice = 15;

        User user1 = new User();
        user1.setNickname(nickName);
        Vote vote = new Vote(nickName, voice);

        String slug = "slug";

        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userDAOMock = Mockito.mockStatic(UserDAO.class)) {
                threadDAOMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
                userDAOMock.when(() -> UserDAO.Info(nickName)).thenReturn(user1);

                ResponseEntity response = threadController.createVote(slug, vote);

                Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
                Assertions.assertEquals(thread, response.getBody());
            }
        }
    }
}