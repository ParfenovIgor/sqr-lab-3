# Lab3 -- Unit testing

[![pipeline status](https://gitlab.com/ParfenovIgor/sqr-lab-3/badges/master/pipeline.svg)](https://gitlab.com/ParfenovIgor/sqr-lab-3/-/commits/master)

## Author

Igor Parfenov

Contact: [@Igor_Parfenov](https://t.me/Igor_Parfenov)

## Unit tests

Created two files with unit tests:

* `ForumControllerTests.java`
* `ThreadControllerTests.java`
